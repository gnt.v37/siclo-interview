import { Directive, ElementRef, HostListener, Input } from '@angular/core';

@Directive({
  selector: '[appBackground]'
})

export class BackgroundDirective {

  private timeout = null;

  constructor(private el: ElementRef) {
  }

  @HostListener('click') onMouseClick() {
    if (this.timeout) {
      clearTimeout(this.timeout);
    }

    this.el.nativeElement.classList.add('zoomOut');

    this.timeout = setTimeout(() => {
      this.el.nativeElement.classList.remove('zoomOut');
    }, 255);
  }

  @Input('background') set background(background) {
    this.el.nativeElement.style.backgroundColor = background;
  }

}
