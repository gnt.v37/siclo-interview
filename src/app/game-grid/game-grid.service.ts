import { Injectable } from '@angular/core';

@Injectable()
export class GameGridService {

  public defaultColors: string[] = ['yellow', 'red', 'blue', 'green'];

  public round: number = 0;

  public getTilesByRound(round: number) {
    const tileColors = [];

    for (let i = 0; i < round + this.defaultColors.length; i++) {
      const tileColor = this.defaultColors[Math.random() * this.defaultColors.length | 0];
      tileColors.push(tileColor);
    }

    return tileColors;
  }

  public shuffleDefaultColors() {
    for (let i = this.defaultColors.length - 1; i > 0; i--) {
      const j = Math.floor(Math.random() * (i + 1));
      const temp = this.defaultColors[i];

      this.defaultColors[i] = this.defaultColors[j];
      this.defaultColors[j] = temp;
    }
  }

  public getTiles() {
    return this.getTilesByRound(this.round);
  }

  public nextRound() {
    this.shuffleDefaultColors();
    this.round += 1;
  }

  public resetRound() {
    this.shuffleDefaultColors();
    this.round = 0;
  }

}
