import { Component, OnInit } from '@angular/core';
import { GameGridService } from './game-grid.service';
import { ScoreBoardService } from '../score-board/score-board.service';

@Component({
  selector: 'app-game-grid',
  templateUrl: './game-grid.component.html',
  styleUrls: ['./game-grid.component.scss']
})
export class GameGridComponent implements OnInit {

  public isStarted: boolean = false;

  public isEnded: boolean = false;

  public isShowTiles: boolean = true;

  public totalScore: number = 0;

  public tiles: string[] = [];

  public selected: string[] = [];

  public isShowTilesTimeOut;

  constructor(
    public gameGridService: GameGridService,
    public scoreBoardService: ScoreBoardService
  ) {
    // Empty
  }

  public ngOnInit() {
    // Empty
  }

  public start() {
    this.isEnded = false;
    this.isStarted = true;
    this.nextRound();
  }

  public pick(tile) {
    if (this.tiles[this.selected.length] === tile) {
      this.selected.push(tile);
      this.scoreBoardService.addScore(this.gameGridService.round);

      if (this.selected.length === this.tiles.length) {
        this.finish();
      }
    } else {
      this.clear();
    }
  }

  private clear() {
    this.resetFlag();
    this.selected = [];
    this.gameGridService.resetRound();
    this.handleClearScore();
  }

  private handleClearScore() {
    this.totalScore = this.scoreBoardService.getTotalScore();
    this.scoreBoardService.clearAllStorage();
  }

  private handleTitleTimeout() {
    clearTimeout(this.isShowTilesTimeOut);

    this.isShowTilesTimeOut = setTimeout(() => {
      this.isShowTiles = false;
    }, 2000);
  }

  private finish() {
    this.selected = [];
    this.scoreBoardService.finishedRound.push(this.gameGridService.round);
    this.nextRound();
  }

  private nextRound() {
    this.tiles = this.gameGridService.getTiles();
    this.gameGridService.nextRound();
    this.isShowTiles = true;
    this.handleTitleTimeout();
  }

  private resetFlag() {
    this.isEnded = true;
    this.isStarted = false;
  }



}
