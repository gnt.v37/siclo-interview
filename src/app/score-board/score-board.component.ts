import { Component, OnInit } from '@angular/core';
import { ScoreBoardService } from './score-board.service';

@Component({
  selector: 'app-score-board',
  templateUrl: './score-board.component.html',
  styleUrls: ['./score-board.component.scss']
})
export class ScoreBoardComponent implements OnInit {

  constructor(
    public scoreBoardService: ScoreBoardService
  ) { }

  ngOnInit() {
  }

}
