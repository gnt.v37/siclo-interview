import { Injectable } from '@angular/core';

@Injectable()
export class ScoreBoardService {

  public scores: number[] = [];

  public finishedRound: number[] = [];

  public addScore(round: number, score: number = 10) {
    if (!this.scores[round - 1]) {
      this.scores[round - 1] = 0;
    }

    this.scores[round - 1] += score;
  }

  public clearAllStorage() {
    this.finishedRound = [];
    this.scores = [];
  }

  public getTotalScore() {
    let total = 0;

    for (let i = 0; i < this.scores.length; i++) {
      if (this.finishedRound[i]) {
        total += this.scores[i];
      }
    }

    return total;
  }

}
