import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { ScoreBoardComponent } from './score-board/score-board.component';
import { GameGridComponent } from './game-grid/game-grid.component';
import { GameGridService } from './game-grid/game-grid.service';
import { ScoreBoardService } from './score-board/score-board.service';
import { BackgroundDirective } from './game-grid/background.directive';

@NgModule({
  declarations: [
    AppComponent,
    ScoreBoardComponent,
    GameGridComponent,
    BackgroundDirective
  ],
  imports: [
    BrowserModule
  ],
  providers: [
    GameGridService,
    ScoreBoardService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
